<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    public $timestamps = true;
    protected $fillable = [
        'title',
        'description',
        'price',
        'category'
    ];
    public static function addPhoto($id, $imageName)
    {
        return DB::table('products')->where('id', '=', $id)->update(['photo' => $imageName]);
    }
    public static function updatePhoto($id, $imageName)
    {
//        $img = DB::table('products')->where('id', '=', $id)->first();
//        var_dump($img);
////        if(!empty($img)){
////            unlink(public_path('/products/' . $img));
////        }
        return DB::table('products')->where('id', '=', $id)->update(['photo' => $imageName]);
    }
    public static function deleteProduct($id)
    {
        $img = DB::table('products')->where('id', '=', $id)->first()->photo;
        return unlink(public_path() . '/products/' . $img);
    }
    public static function isExist($id)
    {
        return DB::table('products')->where('id', '=', $id)->first();
    }

}
