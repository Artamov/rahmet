<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Category extends Model
{
    public static function getAll()
    {
        return DB::table('categories')->get();
    }
    public static function add($title)
    {
        return DB::table('categories')->insert([
            'title' => $title
        ]);
    }

    public static function deleteCat($id)
    {
        return DB::table('categories')
        ->where('id', '=', $id)
        ->delete();
    }
}
