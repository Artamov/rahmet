<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Order extends Model
{
    public $timestamps = true;
    protected $fillable = [
        'name',
        'lastname',
        'email',
        'address',
        'city',
        'country',
        'email_index',
        'phone',
        'delivery_type'
    ];
    public static function add($data)
    {
        return DB::table('orders')->insert([
            'name' => $data['name'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'address' => $data['address'],
            'city' => $data['city'],
            'country' => $data['country'],
            'email_index' => $data['email_index'],
            'phone' => $data['phone'],
            'delivery_type' => $data['delivery_type'],
            'cart_id' => $data['cart_id']
        ]);
    }

}
