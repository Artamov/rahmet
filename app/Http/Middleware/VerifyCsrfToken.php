<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'api/product/add',
        'api/product/delete',
        'api/product/update',
        'api/cart/add',
        'api/order/add',
        'api/categories/add',
        'api/categories/delete'
    ];
}
