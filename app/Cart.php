<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Cart extends Model
{
    public $timestamps = true;
    protected $fillable = [
        'user_id',
        'product_id',
        'count'
    ];

    public static function getProducts($uid)
    {
        $cart = \App\Cart::where('user_id', '=', $uid)
            ->get();
        return $cart;
    }
    public static function getCount($uid)
    {
        $count =\App\Cart::where('user_id', '=', $uid)
            ->get()->count();
        return $count;
    }
}
