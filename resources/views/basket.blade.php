@include('tpl.header')
			
		<!-- NAVIGATION -->
		<nav id="navigation">
			<!-- container -->
			<div class="container">
				<!-- responsive-nav -->
				<div id="responsive-nav">
					<!-- NAV -->
					<ul class="main-nav nav navbar-nav">
						<li><a href="/">Главная</a></li>
						<li><a href="/store">Товары</a></li>
					</ul>
					<!-- /NAV -->
				</div>
				<!-- /responsive-nav -->
			</div>
			<!-- /container -->
		</nav>
		<!-- /NAVIGATION -->

        
        <div class="sectionBasket">
        	@for ($i = 0; $i < count($mycart); $i++)
  								
										
            <div class="blockBasket">
                 <div class="mainBasket">
                        <div class="img">
                                <img src="/products/{{$mycart[$i]->photo}}" alt="">
                        </div>
                        <div class="text">
                            <h1>{{$mycart[$i]->title}}</h1>
                            <p>
                                {{$mycart[$i]->description}}
                            </p>
                        </div>
                        <div class="price">
                            <p>{{$mycart[$i]->price}} <span>тенге</span></p>
                        </div>
                 </div>
                 <div class="func">
                     <div class="countBasket">
                            <div class="qty-label">
									<div class="input-number">
										<input type="number">
										<span class="qty-up">+</span>
										<span class="qty-down">-</span>
									</div>
							</div>
                     </div>
                     <div class="removeBlock" data-id="{{$mycart[$i]->id}}">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                     </div>

                 </div>
                 @endfor
            </div>

        </div>
        

        <div class="divGoYoOrder">
                <a href="/checkout" class="primary-btn order-submit">оформить заказ</a>
        </div>

		<!-- NEWSLETTER -->
		<div id="newsletter" class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<div class="col-md-12">
						<div class="newsletter">
							<p>Оставьте заявку</p>
							<form>
								<input class="input" type="email" placeholder="E-mail">
								<button class="newsletter-btn"><i class="fa fa-envelope"></i> отправить</button>
							</form>
							<ul class="newsletter-follow">
								<li>
									<a href="#"><i class="fa fa-facebook"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-instagram"></i></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /NEWSLETTER -->

	

		@include('tpl.footer')
