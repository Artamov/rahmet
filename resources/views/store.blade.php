@include('tpl.header')

		<!-- NAVIGATION -->
		<nav id="navigation">
			<!-- container -->
			<div class="container">
				<!-- responsive-nav -->
				<div id="responsive-nav">
					<!-- NAV -->
					<ul class="main-nav nav navbar-nav">
						<li><a href="/">Главная</a></li>
					</ul>
					<!-- /NAV -->
				</div>
				<!-- /responsive-nav -->
			</div>
			<!-- /container -->
		</nav>
		<!-- /NAVIGATION -->

		<!-- BREADCRUMB -->
		<div id="breadcrumb" class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<div class="col-md-12">
						<ul class="breadcrumb-tree">
							<li><a href="/">главная</a></li>
							<li><a href="#">все категории</a></li>
						</ul>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /BREADCRUMB -->

		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- ASIDE -->
					<div id="aside" class="col-md-3">
						<!-- aside Widget -->
						<div class="aside">
							<h3 class="aside-title">Категории</h3>
							<div class="checkbox-filter">
								@foreach ($categories as $category)
											
											<div class="input-checkbox">
									<input type="checkbox" id="{{$category->id}}">
									<label for="category-1">
										<span></span>
										{{$category->title}}
										<small>(120)</small>
									</label>
								</div>
										@endforeach
								

							</div>
						</div>
						<!-- /aside Widget -->

					</div>
					<!-- /ASIDE -->
					
					<!-- STORE -->
					<div id="store" class="col-md-9">
						
						<!-- store products -->
						<div class="row">
						
								<!-- product -->
								@foreach ($products as $product)
								<div class="pr">
										  <div class="col-md-4 col-xs-6">
                       				  <a href="product/{{$product->id}}">
                                    <div class="product">
                                        <div class="product-img">
                                            <img src="/products/{{$product->photo}}" alt="">
                                        </div>
                                        <div class="product-body">
                                        <p class="product-category">{{$product->title}}</p>
                                        <h3 class="product-name"><a href="#">{{$product->description}}</a></h3>
                                        <h4 class="product-price">{{$product->price}} тг</h4>
                                    </div>
                                    <div class="add-to-cart">
                                        <button class="add-to-cart-btn" data-id="{{$product->id}}"><i class="fa fa-shopping-cart"></i>в корзину</button>
                                    </div>
                                </div>

                        </a>
                    </div>
								</div>
								@endforeach

								<!-- /product -->


							<div class="clearfix visible-sm visible-xs"></div>

						
						</div>
						<!-- /store products -->

						<!-- store bottom filter -->
						<div class="store-filter clearfix">
							<span class="store-qty">Просмотрите дальше</span>
							<ul class="store-pagination">
								<li class="active">1</li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
								<li><a href="#"><i class="fa fa-angle-right"></i></a></li>
							</ul>
						</div>
						<!-- /store bottom filter -->
					</div>
					<!-- /STORE -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->

	@include('tpl.footer')

