	@include('tpl.header')
	
			
	
						
	
			<!-- NAVIGATION -->
			<nav id="navigation">
				<!-- container -->
				<div class="container">
					<!-- responsive-nav -->
					<div id="responsive-nav">
						<!-- NAV -->
						<ul class="main-nav nav navbar-nav">
							<li><a href="/">Главная</a></li>
							<li><a href="/store">Товары</a></li>
						</ul>
						<!-- /NAV -->
					</div>
					<!-- /responsive-nav -->
				</div>
				<!-- /container -->
			</nav>
			<!-- /NAVIGATION -->
	
		<!-- BREADCRUMB -->
		<div id="breadcrumb" class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<div class="col-md-12">
						<h3 class="breadcrumb-header">Оформление заказа</h3>
						<ul class="breadcrumb-tree">
							<li><a href="/">Главная</a></li>
							<li class="active">оформить заказ</li>
						</ul>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /BREADCRUMB -->

		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">

					<div class="col-md-7">
						<!-- Billing Details -->
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Контактные данные</h3>
							</div>
							<div class="form-group">
								<input class="input name" type="text" name="name" placeholder="Имя">
							</div>
							<div class="form-group">
								<input class="input lastname" type="text" name="lastname" placeholder="Фамилия">
							</div>
							<div class="form-group">
								<input class="input email" type="email" name="email" placeholder="Email">
							</div>
							<div class="form-group">
								<input class="input address" type="text" name="address" placeholder="Адрес">
							</div>
							<div class="form-group">
								<input class="input city" type="text" name="city" placeholder="Город">
							</div>
							<div class="form-group">
								<input class="input country" type="text" name="country" placeholder="Страна">
							</div>
							<div class="form-group">
								<input class="input email_index" type="text" name="email_index" placeholder="Почтовый код">
							</div>
							<div class="form-group">
								<input class="input phone" type="tel" name="phone" placeholder="Телефон">
							</div>
							<div class="form-group">
								<p>Тип доставки</p>
							</div>
							<div class="form-group">
								<select class="delivery_type">
									<option value="Самовывоз">Самовывоз</option>
									<option value="Курьерская доставка">Курьерская доставка</option>
								</select>
							</div>
					 
						</div>
						<!-- /Billing Details -->
						<button class="primary-btn order-submit">оформить заказ</button>
					</div>

					<!-- Order Details -->
					
					<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Оформить заказ</h3>
						</div> 	
						<div class="order-summary">
							<div class="order-col">
								<div><strong>Названия продукта</strong></div>
								<div><strong>Цена</strong></div>
							</div>
							<div class="order-products">
								<div class="order-col">
									<div>Руберойд</div>
									<div>2940 <span>тг</span></div>
								</div>
							
							</div>
							
							<div class="order-col">
								<div><strong>Общая сумма</strong></div>
								<div><strong class="order-total">2940</strong> тг</div>
							</div>
						</div>
					</div>
					
					<!-- /Order Details -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->

		<!-- NEWSLETTER -->
		<div id="newsletter" class="section">
				<!-- container -->
				<div class="container">
					<!-- row -->
					<div class="row">
						<div class="col-md-12">
							<div class="newsletter">
								<p>Оставьте заявку</p>
								<form>
									<input class="input" type="email" placeholder="E-mail">
									<button class="newsletter-btn"><i class="fa fa-envelope"></i> отправить</button>
								</form>
								<ul class="newsletter-follow">
									<li>
										<a href="#"><i class="fa fa-facebook"></i></a>
									</li>
									<li>
										<a href="#"><i class="fa fa-twitter"></i></a>
									</li>
									<li>
										<a href="#"><i class="fa fa-instagram"></i></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<!-- /row -->
				</div>
				<!-- /container -->
			</div>
			<!-- /NEWSLETTER -->
	
		
	
		@include('tpl.footer')
