﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Админ панель</title>

    <!-- Favicon-->
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">


    <!-- Bootstrap Core Css -->
            
    <link href="{{asset('admin/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Waves Effect Css -->

    <link href="{{asset('admin/plugins/node-waves/waves.css')}}" rel="stylesheet" />

    <!-- Bootstrap Select Css -->
    <link href="{{asset('admin/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />


    <!-- JQuery DataTable Css -->

    <link href="{{asset('admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">

    <!-- Custom Css -->

    <link href="{{asset('admin/css/style.css')}}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{asset('admin/css/themes/all-themes.css')}}" rel="stylesheet" />

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <script src="{{asset('admin/js/edit.js')}}"></script>
    
   
</head>

<body class="theme-red">
        <div class="page-loader-wrapper">
                <div class="loader">
                    <div class="preloader">
                        <div class="spinner-layer pl-red">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div>
                            <div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                    <p>Загрузка...</p>
                </div>
            </div>
            <!-- #END# Page Loader -->
            <!-- Overlay For Sidebars -->
            <div class="overlay"></div>
            <!-- #END# Overlay For Sidebars -->
           
            <!-- Top Bar -->
            <nav class="navbar">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                        <a href="javascript:void(0);" class="bars"></a>
                        <a class="navbar-brand" href="index.html">Админ панель</a>
                    </div>
                </div>
            </nav>
            <!-- #Top Bar -->
            <section>
                <!-- Left Sidebar -->
                <aside id="leftsidebar" class="sidebar">    
                    <!-- User Info -->
                    <div class="user-info">
                        <div class="info-container">
                            <div class="email">Здесь вывод почты</div>
                            <div class="btn-group user-helper-dropdown">
                                <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);"><i class="material-icons">input</i>Выйти</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- #User Info -->
                    <!-- Menu -->
                    <div class="menu">
                        <ul class="list">
                        <li class="active">
                        <a href="index">
                            <i class="material-icons">home</i>
                            <span>Главная</span>
                        </a>
                    </li>
                    
                  
                  
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">assignment</i>
                            <span>Товары</span>
                        </a>
                        <ul class="ml-menu">
                          
                            <li>
                                <a href="advanced-form-elements">Удалить и редактировать товар</a>
                            </li>
                            <li>
                                <a href="editors">Добавит товар</a>
                            </li>
                            
                        </ul>
                    </li>
                    <li>
                        <a href="jquery-datatable">
                            <i class="material-icons">view_list</i>
                            <span>Список заказов</span>
                        </a>
                    </li>
                  
                        </ul>
                    </div>
                    <!-- #Menu -->
                    <!-- Footer -->
                    <div class="legal">
                        <div class="copyright">
                            &copy; <a href="javascript:void(0);">Админ панель</a>.
                        </div>
                        
                    </div>
                    <!-- #Footer -->
                </aside>
                <!-- #END# Left Sidebar -->
               
            </section>
        
    <section class="content">
        <div class="container-fluid">           
            <div class="row clearfix formEdit">
                 <form action="http://rahmet.simafood.kz/api/product/update"  method="post" enctype="multipart/form-data">
                        <div class="input-group">
                                    <input type="hidden" class="form-control id"  name="id" placeholder="Названия продукта" aria-describedby="basic-addon1">
                        </div>
                        <div class="input-group">
                                    <input type="text" class="form-control title"  name="title" placeholder="Названия продукта" aria-describedby="basic-addon1">
                        </div>

                        <div class="md-form">
                                    <textarea type="text" id="form10" name="description" placeholder="Описания продукта" class="md-textarea form-control desk" rows="3"></textarea>
                        </div>
                        <br>
                        <div class="input-group">
                            <select name="category" class="category">
                                <option value="1">Категория 1</option>
                                <option value="2">Категория 2</option>
                            </select>
                        </div>
                        <div class="input-group">
                                    <input type="number" class="form-control price" name="price"  placeholder="Цена продукта" aria-describedby="basic-addon1">
                        </div>
                        <input name="photo" type="file" />
                        <br>
                        <input type="submit" class="inptSbm" value="Загрузить">
                </form>
            </div>
           
        </div>
    </section>

   <!-- Jquery Core Js -->
    
   <script src="{{asset('admin/plugins/jquery/jquery.min.js')}}"></script>

    <!-- Bootstrap Core Js -->

    <script src="{{asset('admin/plugins/bootstrap/js/bootstrap.js')}}"></script>

    <!-- Select Plugin Js -->

    <script src="{{asset('admin/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>


    <!-- Waves Effect Plugin Js -->

    <script src="{{asset('admin/plugins/node-waves/waves.js')}}"></script>

    <!-- Custom Js -->
    <script src="{{asset('admin/js/admin.js')}}"></script>


</body>

</html>