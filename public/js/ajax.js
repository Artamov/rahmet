$(function(){
            var uid = localStorage.getItem('uid');
            $('.qwerty1').attr('href', '/basket/' + uid);
            function getCount(){
    var uid = localStorage.getItem('uid');
$.ajax({
                    url: '/api/cart/getCount/' + uid,
                    type: 'GET',
                    success: function(data) {
                         if(data == 'cart empty'){
                        $('.qty').html(0);
                       }else {
                         $('.qty').html(data);
                       }
                       
                    }    
                });  
}
            $('.removeBlock').click(function(){
                var id = $(this).attr('data-id');
                var uid = localStorage.getItem('uid');
                $.ajax({
                    url: '/api/cart/del/' + uid + '/' + id,
                    type: 'GET',
                    
                    success: function(data) {
                        console.log(data);
                         if(data == 'cart empty'){
                        $('.qty').html(0);
                       }else {
                         getCount();
                       }

                    }    
                });
                $(this).parent().parent().remove();
            });


            $('.add-to-cart-btn').click(function(e) {
                var id = $(this).attr('data-id');
                var uid = localStorage.getItem('uid');
                var count = $("input[type='number']").val();
                console.log(count);
                if(!count) count = 1;
                console.log(uid);
                 $.ajax({
                    url: '/api/cart/add',
                    type: 'POST',
                    data: {"uid": uid, "product_id":id, "count": count},
                    success: function(data) {
                        console.log(data);
                        getCount();
                    }    
                });
            });
       
        getCount();


        $('.primary-btn order-submit').click(function(){
            var uid = localStorage.getItem('uid');
             $.ajax({
                    url: '/api/order/add',
                    type: 'POST',
                    data: {
                        "uid": uid, 
                        "name": $('.input name').val(),
                        "lastname": $('.input lastname').val(),
                        "email": $('.input email').val(),
                        "address": $('.input address').val(),
                        "city": $('.input city').val(),
                        "country": $('.input country').val(),
                        "email_index": $('.input email_index').val().

                    },
                    success: function(data) {
                        console.log(data);
                        getCount();
                    }    
                });
        });
});