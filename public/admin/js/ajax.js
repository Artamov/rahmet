$(function(){
    var uid = localStorage.getItem('uid');
    $.ajax({
        type: 'GET',
        url: 'http://rahmet.simafood.kz/api/product/all?uid=' + uid,
        success: function(data) {
            var product = "";
            if(data.uid){
                localStorage.setItem('uid', data.uid);
                var view = data.products
            }else {
                view = data;
            }
            view.forEach(element => {
                console.log(element);
                product += `
                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                        
                        <img src="http://rahmet.simafood.kz/products/${element.photo}">                            
                        <div class="caption">
                            <div class="desc" style="height: 100px; overflow-y: scroll;">
                                <h3>${element.title}</h3>
                                <h5><span>${element.price}</span> тенге</h5>
                                <p style="color: #000">${element.description}</p>
                            </div>
                            <p><a class="deletebtn btn btn-primary" data-id="${element.id}" role="button">Удалить</a> 
                            <a class="btn btn-default editform" data-id="${element.id}" role="button">Редактировать</a></p>
                        </div>
                        </div>
                    </div>
                `;
                
            });
            $('.dell').html(product);       
            
            $('.deletebtn').on('click', function(e){
                e.preventDefault();
                 var id = $(this).attr('data-id');
                 $.ajax({
                        type: "POST",
                        url: "http://rahmet.simafood.kz/api/product/delete",
                        data: {
                                'uid': uid, 
                                'id': id
                        },
                        success: function(msg){
                            console.log( "Прибыли данные: " + msg);
                            window.location.reload();
                        }
                  });
            });   

            $('.editform').on('click', function(e){
                e.preventDefault();
                 var id = $(this).attr('data-id');
                 $.ajax({
                        type: "GET",
                        url: "http://rahmet.simafood.kz/api/product/"+id,
                        data: {
                                'uid': uid, 
                                'id': id
                        },
                        success: function(msg){
                            console.log( "Прибыли данные: " + msg);
                            var jsp = JSON.stringify(msg);
                            localStorage.setItem('edit', jsp);
                            location.href = 'edit';
                        }
                  });
            }); 
            
        }
    })

})