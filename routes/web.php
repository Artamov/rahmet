<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

Route::group(['prefix' => 'api/product/',  'middleware' => 'cors'], function () {

    // получить все
    Route::get('/all', ['middleware' => 'cors', function(Request $request){
        $products = \App\Product::all();
        $uid = \Illuminate\Support\Facades\Input::get('uid');
        if(empty($uid)  || (strlen($uid) < 5) ){
            return response(['aaa'=> $uid, 'uid' => str_random(), 'products' => $products]);
        }
        return response($products, 200);
    }]);
    // получить по id
    Route::get('/{id}/{uid}', function(Request $request, $id){
        if(empty($id)){
            return response('id empty!', 400);
        }
        if($product = \App\Product::where('id', '=', $id)->first()){
            return response($product, 200);
        }
    });
    //поиск
    Route::get('/search', function(Request $request){
        // $valid = Validator::make($request->all(), [
        //     'word'=> 'required',
        // ]);
        // if( $valid->fails() ) {
        //     return response()->json($valid->errors(), 400);
        // }
        $word = \Illuminate\Support\Facades\Input::get('word');
        if($products = \App\Product::where('title', 'LIKE', "%$word%")->get()){
            $categories = \App\Category::getAll();
            return view('store', compact('products', 'categories'));
        }
    });
    // Добавление
    Route::post('/add', ['middleware' => 'cors', function(Request $request){

        $valid = Validator::make($request->all(), [
            'title'=> 'required',
            'description' => 'required',
            'price' => 'required|integer',
            'category' => 'required',
            'photo' => 'required|image|mimes:jpg,png,jpeg,gif,svg'
        ]);

        if ( $valid->fails() ) {

            return response()->json($valid->errors(), 400);
        }

        try {
            $imageName = time().'.'.$request->photo->getClientOriginalExtension();
            $request->photo->move(public_path('products'), $imageName);
            $id = \App\Product::create($request->all())->id;
            \App\Product::addPhoto($id, $imageName);
            return response('Success added', 200);
        }catch (\Exception $e){
            return response($e->getMessage(), 400);
        }

    }]);

    // редактирование
    Route::post('/update',['middleware' => 'cors', function(Request $request){

        $valid = Validator::make($request->all(), [
            'id' => 'required',
            'title'=> 'required',
            'description' => 'required',
            'price' => 'required|integer',
            'category' => 'required|integer',
        ]);

        if ( $valid->fails() ) {
            return response()->json($valid->errors(), 400);
        }

        try {

            if(!\App\Product::isExist($request->id)){
                return response('product not fount', 400);
            }
            \App\Product::where('id', '=', $request->id)->update($request->all());
            if(!empty($request->photo)){
                $imageName = time().'.'.$request->photo->getClientOriginalExtension();
                \App\Product::updatePhoto($request->id, $imageName);
                $request->photo->move(public_path('products'), $imageName);

            }
            return response('Success updated', 200);
        }catch (\Exception $e){
            return response($e->getMessage(), 400);
        }

    }]);

    // Удаление
    Route::post('/delete', ['middleware' => 'cors', function(Request $request){
        try {
            if ( empty($request->id) ) {
                return response('Id empty', 400);
            }
            if(\App\Product::deleteProduct($request->id)){
            }
            if(\App\Product::where('id', '=', $request->id)->delete()){

                return response('product deleted', 200);
            }else { return response("product no found!", 200);}

        }catch(\Exception $e){

        }


    }]);
});



Route::group(['prefix' => 'api/cart/',  'middleware' => 'cors'], function () {
    Route::post('/add', function(Request $request){
        if(empty($request->product_id) || empty($request->uid)){
            return response('product_id empty', 400);
        }
        if(\App\Cart::where('user_id', '=', $request->uid)
        ->where('product_id', '=', $request->product_id)->first() && \App\Cart::where('user_id', '=', $request->uid)
        ->where('product_id', '=', $request->product_id)->first()->count == $request->count)
        {
            return response('!!!', 400);
        }elseif(\App\Cart::where('user_id', '=', $request->uid)
        ->where('product_id', '=', $request->product_id)->first() && \App\Cart::where('user_id', '=', $request->uid)
        ->where('product_id', '=', $request->product_id)->first()->count != $request->count) {
            \App\Cart::where('product_id', '=', $request->product_id)
            ->where('user_id', '=', $request->uid)->update([
                'count' => $request->count
            ]);
        }else {
             \App\Cart::create([
                'product_id' => $request->product_id,
                'user_id' => $request->uid,
                'count' => $request->count
            ]);
        }
        try {
           
            return response('products success cart added', 200);
        }catch ( \Exception $e ) {
            return response($e->getMessage(), 400);
        }

    });
    Route::get('/get', function(Request $request){
        if($cart = \App\Cart::getProducts(\Illuminate\Support\Facades\Input::get('uid'))){
            return response($cart, 200);
        }else {
            return response('cart empty', 200);
        }

    });

    Route::get('/del/{uid}/{pid}', function(Request $request, $uid, $pid){
        if(\App\Cart::where('user_id', '=', $uid)->where('product_id', '=', $pid)->delete()){
            return response('sucees', 200);
        }else {
            return response('cart empty', 200);
        }

    });

     Route::get('/getCount/{uid}', function(Request $request, $uid){
        if($cart = \App\Cart::getCount($uid)){
            return response($cart, 200);
        }else {
            return response('cart empty', 200);
        }

    });

});

Route::group(['prefix' => 'api/order/',  'middleware' => 'cors'], function () {
    Route::post('/add', function(Request $request){
        $valid = Validator::make($request->all(), [
            'name' => 'required',
            'lastname'=> 'required',
            'email' => 'required|email',
            'address' => 'required',
            'city' => 'required',
            'country' => 'required',
            'email_index' => 'required',
            'phone' => 'required',
            'delivery_type' => 'required'
        ]);
        if ( $valid->fails() ) {
            return response()->json($valid->errors(), 400);
        }
        try{
            if(\App\Order::create($request->all())){
            	return response()->json("success order added", 200);
            }

        }catch (\Exception $e){

        }
    });
});


Route::group(['prefix' => 'api/categories',  'middleware' => 'cors'], function () {
    Route::get('/get', function(){
        return response()->json(\App\Category::getAll(), 200);
    });

    Route::post('/add', function(Request $request){
        $valid = Validator::make($request->all(), [
            'title' => 'required'
        ]);
        if ( $valid->fails() ) {
            return response()->json($valid->errors(), 400);
        }
        try{
            if(\App\Category::add($request->all())){
                return response()->json("success category added", 200);
            }

        }catch (\Exception $e){

        }
    });

    Route::post('/delete', function(Request $request){
        $valid = Validator::make($request->all(), [
            'id' => 'required'
        ]);
        if ( $valid->fails() ) {
            return response()->json($valid->errors(), 400);
        }
        try{
            if(\App\Category::deleteCat($request->id)){
                return response()->json("success category deleted", 200);
            }

        }catch (\Exception $e){

        }
    });

});





Route::get('/', function(){
    $products = \App\Product::inRandomOrder()->limit(2);
    $categories = \App\Category::getAll();
    $cartCount = 1;
    return view('index', compact('products', 'categories', 'cartCount'));
});
Route::get('/basket/{uid}', function(Request $request, $uid){
    $products = \App\Product::inRandomOrder()->limit(2);
    $categories = \App\Category::getAll();
    $cartCount = 1;
    $cart = \App\Cart::getProducts($uid);
    $mycart = collect();
    foreach ($cart as $car) {
        $mycart->push(\App\Product::where('id', '=', $car->product_id)->first());
    }
    return view('basket', compact('products', 'categories', 'cartCount', 'mycart'));
});
Route::get('/checkout', function(){
     $products = \App\Product::get();
    $categories = \App\Category::getAll();
    $cartCount = 1;
    return view('checkout', compact('products', 'categories', 'cartCount'));
});
Route::get('/product/{id}', function(Request $request, $id){
    $categories = \App\Category::getAll();
    $cartCount = 1;
    $product = \App\Product::where('id', '=', $id)->first();
    return view('product', compact('product', 'categories', 'cartCount'));
});
Route::get('/store', function(){
    $products = \App\Product::get();
    $categories = \App\Category::getAll();
    $cartCount = 1;
    return view('store', compact('products', 'categories', 'cartCount'));
});



Route::group(['prefix' => 'admin/',  'middleware' => 'cors'], function () {
    Route::get('index', function(){
        return view('admin/index', []);
    });
    Route::get('editors', function(){
        return view('admin/editors', []);
    });
    Route::get('advanced-form-elements', function(){
        return view('admin/advanced-form-elements', []);
    });
    Route::get('jquery-datatable', function(){
        return view('admin/jquery-datatable', []);
    });
    Route::get('edit', function(){
        return view('admin/edit', []);
    });
});


